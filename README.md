# gwcloud_auth_helm

Helm chart repository for gwcloud_auth

## Assumptions
- Helm3 is [installed](https://helm.sh/docs/intro/install/)
- Relevant docker images and container image registry exists
- Relevant vault access and secrets exists
- Relevant storage access and storage solutions exist

## Repo Initialisation
- Initialise helm repo by executing `helm create gwcloud_auth`
- Move helm output by `mv gwcloud_auth/* . && mv gwcloud_auth/.helmignore .`
- Cleanup `rmdir gwcloud_auth`

## Chart values
Dynamic variables declared and initialised in [values.yaml](./values.yaml)
| Variable | Default | Comment |
| --- | --- | --- |
| `deployment.name` | `gwcloud-auth` | Name value of k8s deployment resource |
| `image.repository` | `nexus.gwdc.org.au/docker/gwcloud_auth` | Container image repository |
| `image.tag` | `""` |  Main chart application `gwcloud-auth` image tag controlled through `appVersion` in [Chart.yaml](./Chart.yaml) |
| `vaultAnnotations.role` | `gwcloud-auth` | Role configured at the vault server `vault.gwdc.org.au` |
| `vaultAnnotations.secrets` | [] | list of vault kv engines used as a reference for populating container env variables |
|||

## Architecture
TBA


## Prerequisites
```bash
# Vault dependencies
#   Vault cli required
#   cli must have network access to vault
vault login -address=$VAULT_HTTPS_FQDN -method=github -token=$ACCESS_TOKEN

# Add vault policy
tee auth.policy.hcl <<EOF
# Read auth deployment config
path "kv/gwcloud/auth"
{
  capabilities = ["list", "read"]
}

# Read gwcloud common credentials
path "kv/gwcloud/common"
{
  capabilities = ["list", "read"]
}
EOF
vault policy write auth auth.policy.hcl

# Sanity Check
vault policy read auth

# Add vault kubernetes role
vault write auth/kubernetes/role/auth \
    bound_service_account_names=gwcloud-auth \
    bound_service_account_namespaces=gwcloud \
    policies=default,auth \
    ttl=1h

# Sanity Check
vault read auth/kubernetes/role/auth
```

## Support
TBA

## ToDo
- [x] Integrate Vault secrets.
- [x] Proof of concept deployment.
- [x] CI for testing the helm chart.
- [x] Make port definitions dynamic.
- [ ] CD for deploying packaged charts to `https://nexus.gwdc.org.au/#browse/browse:helm`
- [ ] CD Runtime test to dev k8s cluster.
